# -*- coding: utf-8 -*-

#!/usr/bin/env python
# C:\SOFT\parseRepo>pip --proxy http://login:password@proxy-internet-mtl.pub.desjardins.com:8080 install lxml

from lxml import etree
import configparser
import re
import os
import json
import datetime


config = configparser.ConfigParser()
config.read('./parseRepo.ini')


xmlMsgList = []
resultFileHandle = '' 

# load iso20022 repository
context = etree.parse(config['FOLDER']['RepoFolder'] + config['ISO']['ISORepository'])
baList = config['ISO']['baList']

outputDir = config['FOLDER']['outputFolder']  

# Create DIR if it doesnt exist
if not os.path.exists(outputDir):
    os.makedirs(outputDir)


# Message to parse
#nexoMessage = config['ISO']['ISOMessage']
# resultFile
#resultFile = "./"+nexoMessage+".tsv"


# function to indent using tabs according to the xml depth level
# inputs the depth level as an int
# returns a string of tabs
def indentLevel(level):
    tabIndent = ""
    for i in range(1, level):
        tabIndent = "\t" + tabIndent
    return tabIndent

# inputs : a XML element
# returns a string representing multiplicity with format [0..n] based on minCoccurs and maxOccurs attributes
def getMultiplicity(elementResp):
    # 10..* by default
    multiplicity = "[0..*]"
    #print(' Elem Response for multiplicity : ' + str(elementResp.get("minOccurs")) +'--' +str(elementResp.get("maxOccurs"))   )
    if elementResp.get("minOccurs") is not None:
        multiplicity = "[" + elementResp.get("minOccurs") + ".."
        if elementResp.get("maxOccurs") is not None:
            multiplicity = multiplicity + elementResp.get("maxOccurs") + "]"
        else:
            multiplicity = multiplicity + "*]"
    else:
        if elementResp.get("maxOccurs") is not None:
            multiplicity = "[0.." + elementResp.get("maxOccurs") + "]"
    #print(' Compted  multiplicity : ' + multiplicity   )
 
    return multiplicity

# inputs : resultFileHandle a handle on the file where to write the result, elementResp a XML element, context the ISO20022 repository as a tree, level the depth as an int, fullname a string to calculate the fullname recusrisively
# outputs the xml nodes to the console depth\tnode fullpath\ttype of element(msgComponent or datatype or codeset)\txml long name\
# writes to a file the same output


def findMessageComponent(resultFileHandle, elementResp, context, level, fullname):
    global xmlMsgList
    global line
    
    fullname2 = fullname + "/" + elementResp.get("xmlTag")
    level = level + 1
    xpathExp="//datatypes[@id='"+elementResp.get("type") + "']|//messageComponents[@id='"+elementResp.get("type") + "']|//externalSchemas[@id='"+elementResp.get("type") + "']"
    
    for dataTypeDef in context.xpath(xpathExp):
        if (line % 100) == 0:
            now = datetime.datetime.now()
            print( now.strftime("%Y-%m-%d %H:%M:%S") + '-  Processing extraction at  ' + fullname2  + ' at index ' + str(line))
        line += 1
  
        if dataTypeDef.tag == "messageComponents":
            for msgCompDef in context.xpath("//messageComponents[@id='"+elementResp.get("type") + "']"):
                #print(str(level) + "\t" + fullname2.strip() + "\t" + getMultiplicity(elementResp) +
                #      "\t" + msgCompDef.get("name").strip() + "\t" + elementResp.get("name").strip())
                #resultFileHandle.write(fullname2.strip() + "\t" + getMultiplicity(elementResp) +
                #           "\t" + msgCompDef.get("name").strip() + "\t" + elementResp.get("name").strip() + "\n")
                xpath = fullname2.strip()
                xpathList = re.split('[/]', xpath)
                curLevel = len(xpathList) - 1
                multiplicity = getMultiplicity(elementResp)
                xmltype = msgCompDef.get("name").strip()
                xmlName = elementResp.get("name").strip()
                xmlMsgList.append({"Xpath" : xpath, "Level" : curLevel-1 ,"Tag" :xpathList[curLevel],"Name" :xmlName, "Mult" :multiplicity,"Type" :xmltype,"Xpathlist" :xpathList})     
  
                elements2Msg = list(msgCompDef)
                for element2Msg in elements2Msg:
                    if element2Msg.tag == "elements":
                        findMessageComponent(
                            resultFileHandle,element2Msg, context, level, fullname2)
        else:
            #Datatype or Codeset
            
            
            for dataTypeDef in context.xpath("//datatypes[@id='"+elementResp.get("type") + "']"):
                #print(str(level) + "\t" + fullname2.strip() + "\t" + getMultiplicity(elementResp) +
                #      "\t" + dataTypeDef.get("name").strip() + "\t" + elementResp.get("name").strip())
                xpath = fullname2.strip()
                xpathList = re.split('[/]', xpath)
                curLevel = len(xpathList) - 1
                multiplicity = getMultiplicity(elementResp)
                xmltype = dataTypeDef.get("name").strip()
                xmlName = elementResp.get("name").strip()
                xmlMsgList.append({"Xpath" : xpath, "Level" : curLevel-1 ,"Tag" :xpathList[curLevel],"Name" :xmlName, "Mult" :multiplicity,"Type" :xmltype,"Xpathlist" :xpathList})     
            # for the envelope in supplementary data
            for extSchemaTypeDef in context.xpath("//externalSchemas[@id='"+elementResp.get("type") + "']"):
                
                #print(str(level) + "\t" + fullname2.strip() + "\t" + getMultiplicity(elementResp) +
                #       "\t" + dataTypeDef.get("name").strip() + "\t" + elementResp.get("name").strip())
                xpath = fullname2.strip()
                xpathList = re.split('[/]', xpath)
                curLevel = len(xpathList) - 1
                multiplicity = getMultiplicity(elementResp)
                xmltype = extSchemaTypeDef.get("name").strip()
                xmlName = elementResp.get("name").strip()
                xmlMsgList.append({"Xpath" : xpath, "Level" : curLevel-1 ,"Tag" :xpathList[curLevel],"Name" :xmlName, "Mult" :multiplicity,"Type" :xmltype,"Xpathlist" :xpathList})     

                
                
                
                
    level = level - 1

# main function ; need to choose below the message targeted like AcceptorAuthorisationRequestV07
#input : resultFileHandle a handle on the file where to write the result

#@do_profile(follow=[findMessageComponent])
#def main(resultFileHandle):
def main():
    global xmlMsgList
    global line
    for msgDef in context.xpath("/xmi:XMI/mx:MXModel/catalogue/messageDefinitions", namespaces={'mx': 'http://www.swift.com/standards/mx/1.5.0',
                                                                                                'xmi': 'http://www.omg.org/XMI',
                                                                                                'view': 'http://www.standards.swift.com/viewmodel/2010/',
                                                                                                'xsi': 'http://www.w3.org/2001/XMLSchema-instance'}):
        level = 1
        fullname = "Document/"
        nexoMessageID =  msgDef.get("messageIdentifier")  
        nexoMessageIDList = re.split('[.]',nexoMessageID)
        
        if nexoMessageIDList[0] in baList:
            #cp = cProfile.Profile()
            # cp.enable()
            fullname=fullname+msgDef.get("xmlTag")
            #resultFileHandle.write(fullname.strip() + "\t" + "[1..1]" +
            #                   "\t" + msgDef.get("name").strip() + "\t" + msgDef.get("name").strip() + "\n")
            line = 1
           
            elementsMsg = list(msgDef)
            xmlMsgList = []
            xmlTag =  msgDef.get("xmlTag").strip()
            xmltype = msgDef.get("name").strip()
            xmlName = msgDef.get("name").strip()
            xpath = fullname.strip()
            xpathList = re.split('[/]', xpath)
            curLevel = len(xpathList) - 1
            multiplicity = "[1..1]"
            print(str(curLevel-1) + "\t" + fullname.strip() + "\t" + multiplicity +
                      "\t" +xmlName + "\t" + xmlName)
            xmlMsgList.append({"Xpath" : xpath, "Level" : curLevel-1 ,"Tag" :xmlTag ,"Name" :xmlName, "Mult" :multiplicity,"Type" :xmltype,"Xpathlist" :xpathList})     
            
            now = datetime.datetime.now()
            print( now.strftime("%Y-%m-%d %H:%M:%S") + '-  Processing message   ' + nexoMessageID  + '  ' + xpath )
    
            
            for elementResp in elementsMsg:
                if elementResp.tag == "elements":
                    findMessageComponent(
                        resultFileHandle, elementResp, context, level, fullname)
                # cp.disable()
                # cp.print_stats()
            fcode = open(outputDir + nexoMessageID + '.json' , 'w')
            
            json.dump(xmlMsgList, fcode)
            
            fcode.close()
            
        #f.close
        

#f = open(resultFile, 'w')
#main(f)
now = datetime.datetime.now()       
print(now.strftime("%Y-%m-%d %H:%M:%S") + ' - Starting extracting repository data  ')

main()

now = datetime.datetime.now()         
print('<< end of processing >>  - '+ now.strftime("%Y-%m-%d %H:%M:%S"))

#f.close()

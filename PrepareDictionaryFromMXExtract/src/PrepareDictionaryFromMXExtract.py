'''
Created on Nov 11, 2018

@author: DHC3606

  This script create a global dictionary from the message data extracted from the repository, 

  The extract is the list of the element of a message, each element is describe by a dictionary. 
  
  Each element are read and a new entry is created, the key is the XPATH. 




'''
import re
import os
import json
import subprocess
import time

from docx import Document
from docx import *
from docx.shared import Pt
#from docx.enum.section import WD_ORIENT
from docx.shared import Cm
from docx.text.tabstops import TabStop, TabStops


def remove_duplicates(l):
    newList = []
    for listItem in l:
        if not listItem in newList:
            newList.append(listItem) 
    return newList






def parseParam():
    paramValue = re.split(' ', paramLine)
    global excludeList 
    global inputFile
    global path
    
    global outpathDictT_to_N
    global outpathDictN_to_T
    global outpathDictNexo
    global documentName
    global maxLevelParm
    global Save
 
    
    excludeList = []
    inputFile = ''
    
    maxLevelParm = 99
    for index in range(len(paramValue)):
        if len(paramValue[index]) != 0:
            if paramValue[index][-1] == '\n':
                paramValue[index] = paramValue[index][:-1]
        if len(paramValue[index]) != 0:
            if paramValue[index][-1] == '\r':               # In case of file created on windows and acessed on Unix/Linux/MAC
                paramValue[index] = paramValue[index][:-1] 
        splitParam = re.split('=', paramValue[index])
        if splitParam[0] == 'SOURCE':
            path = splitParam[1]
        
        if splitParam[0] == 'DESTDICT_Tag_to_Name':
            outpathDictT_to_N = splitParam[1]
        if splitParam[0] == 'DESTDICT_Name_to_Tag':
            outpathDictN_to_T = splitParam[1]   
        if splitParam[0] == 'DESTDICT_Nexo':
            outpathDictNexo = splitParam[1]       
            
        if splitParam[0] == 'FILE' :
            inputFile = splitParam[1]
        if splitParam[0] == 'SAVE' :
            Save = True

    print(' Param parsed: ' + paramLine )        
            
            

path = ''

outpathDictT_to_N = ''
outpathDictN_to_T = ''
outpathDictNexo = ''
 
excludeList = []
fromTag = ''
Save = False

inputFile = ''

fparam = open('PrepareDictionaryFromMXExtract.ini')

documentName = ''
xmlTagList = [] 
xmlNexoList = [] 
messageData = []
xmlTagDictT_to_N = {}
xmlTagDictN_to_T = {}
xmlNexoDict = {}

print('Starting Tag Name Table generation')

for paramLine in fparam:
    if not paramLine[0] == '#':
        parseParam()
    if not inputFile == '' :
        fobj = open(path+inputFile)
        print('Opening '+path+ inputFile )
        
     
        messageData = json.loads(fobj.read() )
        for elementDict in messageData :
            print( 'Adding : ' + elementDict["Name"] + ' ' ,elementDict["Tag"] )
            xmlTagList.append([elementDict["Tag"],elementDict["Name"]])
            xmlNexoDict[elementDict["Xpath"]] = elementDict 
            

        inputFile = ''
     
    if Save:      
        # remove duplicate 
        print('==> Saving dictionaries  ')
        xmlTagList = remove_duplicates(xmlTagList)
       
        #                
        for listItem in xmlTagList:
            xmlTagDictN_to_T[listItem[1]]=listItem[0]        
            xmlTagDictT_to_N[listItem[0]]=listItem[1]  
      
        #conserver dans le fichier  
        
        
        fcode = open(outpathDictN_to_T , 'w')
        
        json.dump(xmlTagDictN_to_T, fcode)
        
        fcode.close()
        
        fcode = open(outpathDictT_to_N , 'w')
        
        json.dump(xmlTagDictT_to_N, fcode)
        
        fcode.close()
        
        fcode = open(outpathDictNexo , 'w')
        
        json.dump(xmlNexoDict, fcode)
        
        fcode.close()
        xmlTagList = [] 
        xmlNexoList = [] 
        xmlTagDictT_to_N = {}
        xmlTagDictN_to_T = {}
        xmlNexoDict = {}
        Save = False
        
      
#topCommandFile.close()
print('<< end of processing >>')
#!/usr/local/bin/python2.7
# encoding: utf-8
'''
Created on 21 mai 2018

@author: martinprovost



[ to do : add list of key word ]




And be patient, during processing, a line is printed at every 100 rows, and it can take 5 to 10 minutes to process 100 rows. 
Processing of the acquirer MUG may take more than 1h30, the acceptor batch transfert is huge and take more than 1 hour. 

'''

from __future__ import print_function # Py2 compat

import re
import os
import json
import subprocess
import time
import datetime

from collections import namedtuple
import sys
from docx import Document
from docx import *
from docx.shared import Pt
#from docx.enum.section import WD_ORIENT
from docx.shared import Cm
from docx.shared import RGBColor
from docx.text.tabstops import TabStop, TabStops
from docx.enum.text import WD_PARAGRAPH_ALIGNMENT, WD_UNDERLINE

import lxml
from lxml import etree


from pprint import pprint


def parseParam():
    print(' Parsing : '+paramLine )
    paramValue = re.split(' ', paramLine)
    
    print(' Param Value  : '+  str(paramValue) )
  
     
    global inputMessage
    global msgData  
    global dataPath
    global mugDocument
    global oldMugDir
    global newMugDir 
    global oldCtrlDir
    global newCtrlDir 
    global pathDictN_to_T
    global pathDictNexo
    global endPointList
    global flagOrColumn
    global diffReportDir
     

     
    for index in range(len(paramValue)):
        if len(paramValue[index]) != 0:
            if paramValue[index][-1] == '\n':
                paramValue[index] = paramValue[index][:-1]
        if len(paramValue[index]) != 0:
            if paramValue[index][-1] == '\r':
                paramValue[index] = paramValue[index][:-1]
        splitParam = re.split('=', paramValue[index])
       
        
        if splitParam[0] == 'MESSAGE' :
            inputMessage = splitParam[1]
            flagOrColumn = False     
        if splitParam[0] == 'MSGDATA':
            msgData = splitParam[1]  
        if splitParam[0] == 'MSGDATADIR':
            dataPath = splitParam[1]  
        if splitParam[0] == 'MUGDOC':
            mugDocument  = splitParam[1]  
        if splitParam[0] == 'OLDMUGDIR':
            oldMugDir = splitParam[1]  
        if splitParam[0] == 'NEWMUGDIR':
            newMugDir = splitParam[1]  
        if splitParam[0] == 'OLDCTRLDIR':
            oldCtrlDir = splitParam[1]  
        if splitParam[0] == 'NEWCTRLDIR':
            newCtrlDir = splitParam[1]     
            
        if splitParam[0] == 'DICT_Name_to_Tag':
            pathDictN_to_T = splitParam[1]    
        if splitParam[0] == 'DICT_Nexo':
            pathDictNexo = splitParam[1]            
        if splitParam[0] == 'ENDPOINT':
            endPointList.append(splitParam[1])
        if splitParam[0] == 'FLAGOR':
            flagOrColumn = True   
        if splitParam[0] == 'DIFFREPORTDIR':
            diffReportDir = splitParam[1]        
            

    print(' Param parsed: ' + paramLine )        


def remove_row(table, row):
    tbl = table._tbl
    tr = row._tr
    tbl.remove(tr)            

def  addXMLEntryInMsgTable():   
    global msgTableIndex
    global maxTableEntry
     
    global xmlTableIndex 
    global maxXmlTableEntry
    global tabsSetting
    global endPointList
    global isEndPointActive 
    global endPointXpath 
    global diffXpath
    global curRowStatusFlags  
    global curRowControlFlags
    
    
    #print( 'Line to add :' + diffXpath.line )
    #print( '   at index :'+ str(msgTableIndex))
    # add an entry a the end with add_row    
    newRow = msgTable.add_row()
    # copy the empty row before common row
    # this is done directly using lxml.xtree function 
    #  
    _newRow = newRow._tr
    _previousRow = msgTable.rows[msgTableIndex-1]._tr
    _previousRow.addnext(_newRow) # lxml.tree function to add a sibling
    # delete row at end, we don't need it anymore
    #remove_row(msgTable, msgTable.rows[-1])
    
    # Now, since a Row has been added, msgTableIndex now point to new entry 
    # Initialise new row with new entry, all in green undeline to mark new. 
 

    #wrkRow = msgTable.rows[msgTableIndex]
    wrkRow = newRow
    for wrkCell in wrkRow.cells: #  
        for wrkPar in wrkCell.paragraphs:
            if wrkPar.style != 'MessageTable':  
                wrkPar.style = 'MessageTable'  
    
    #xmlDictItem = xmlTagDictNexo[diffXpath.line]
    # compute the level
    #elementList = re.split('[/]', diffXpath.line)
    #print(' Element list :' + elementList )
    #curLevel = len(elementList) - 2
    curLevel =  xmlDictItem["Level"]       
    ps = wrkRow.cells[msgTableLevelColIndex].paragraphs
    # Level               
    runTmp = ps[0].add_run(str(curLevel))
    runFont = runTmp.font 
    runFont.color.rgb = greenColor
    runFont.name = 'Arial'
    runFont.size= Pt(8) 
    runFont.underline = WD_UNDERLINE.DOUBLE
    ps_fmt = ps[0].paragraph_format
    ps_fmt.alignment = WD_PARAGRAPH_ALIGNMENT.CENTER 

    
    
    
    # Set Name, with the tab, according to Level
 
    tabStr = ''
    if curLevel >= 1:
        for n in range(curLevel-1):
            tabStr = tabStr + '\t'
    #row_cells[1].text = tabStr + xmlDictItem[2]
    ps = wrkRow.cells[msgTableNameColIndex].paragraphs
    #font.color = blackColor
    
    
    #
    #  And soft Hyphen before capital letter in message name
    #
    #msgNewName = xmlDictItem[3].encode('utf-8')
    '''
    Doesn't work, the unicode soft hyphen is not converted to offices hypen ! 
    msgNewName = xmlDictItem[3]
    n = 0
    maxLenName =  len(msgNewName)
    while (n < maxLenName):
        c = msgNewName[n]
        if c.isupper() and n > 4:
            msgNewName = msgNewName[:n] + u'\xAD'  +msgNewName[n:] 
            n += 2
        n += 1  
    '''
    
    
    runTmp = ps[0].add_run(tabStr + xmlDictItem["Name"]) 
    runFont = runTmp.font 
    runFont.bold = True 
    runFont.color.rgb = greenColor
    runFont.name = 'Arial'
    runFont.size= Pt(8) 
    runFont.underline = WD_UNDERLINE.DOUBLE
    
 
    #     
    # Set the leftIndent according to level.  This in case a of too big name, inserting a soft hyphen will set new line just under the first letter
    #
    #               
   
    
    leftIndent = 0
    if curLevel >= 2:   
        leftIndent = (curLevel-1) * Cm(0.2)  
    
    #msgTable.rows[msgTableIndex].cells[msgTableNameColIndex].paragraphs[0].paragraph_format.tab_stops = tabsSetting
    wrkPf = wrkRow.cells[msgTableNameColIndex].paragraphs[0].paragraph_format
    wrkPf.first_line_indent = -leftIndent 
    wrkPf.left_indent = leftIndent
 
    
  
    # Set Mult
    ps = wrkRow.cells[msgTableMultColIndex].paragraphs
    runTmp = ps[0].add_run(xmlDictItem["Mult"]) 
    runFont = runTmp.font 
    runFont.color.rgb = greenColor
    runFont.name = 'Arial'
    runFont.size= Pt(8) 
    runFont.underline = WD_UNDERLINE.DOUBLE
    ps_fmt = ps[0].paragraph_format
    ps_fmt.alignment = WD_PARAGRAPH_ALIGNMENT.CENTER 
    
    # Empty rule set to blank, but formatted in case of someone add a rule
    ps = wrkRow.cells[msgTableRuleColIndex].paragraphs
    runTmp = ps[0].add_run('') 
    runFont = runTmp.font 
    runFont.color.rgb = greenColor
    runFont.name = 'Arial'
    runFont.size= Pt(8) 
    runFont.underline = WD_UNDERLINE.DOUBLE
    # Empty constraint set to blank, but formatted in case of someone add a contraint
    ps =wrkRow.cells[msgTableCstrColIndex].paragraphs
    runTmp = ps[0].add_run('') 
    runFont = runTmp.font 
    runFont.color.rgb = greenColor
    runFont.name = 'Arial'
    runFont.size= Pt(8) 
    runFont.underline = WD_UNDERLINE.DOUBLE
    ps_fmt = ps[0].paragraph_format
    ps_fmt.alignment = WD_PARAGRAPH_ALIGNMENT.CENTER 
    
    #
    if 'EP' in curRowControlFlags:   # this is an end point 
                        cellText = curMsgRow.cells[msgTableDescColIndex].text
                        if 'See MDR for sub elements' not in cellText:
                            # take last paragraph with component type 
                            wrkLastPar = curMsgRow.cells[msgTableDescColIndex].paragraph[0]
                            # Add the text before 
                            newPar = wrkLastPar.insert_paragraph_before(text='See MDR for sub elements') 
                            
    
    
    # Tag and type in bottom of usage
    lastc = xmlDictItem["Type"][-1]        
    if lastc.isdigit():
        typeElem = '>'
    else:
        typeElem = '>::'+xmlDictItem["Type"]      
    ps = wrkRow.cells[msgTableDescColIndex].paragraphs
    runTmp = ps[0].add_run('<'+xmlDictItem["Tag"]+typeElem) 
    runFont = runTmp.font 
    runFont.color.rgb = greenColor
    runFont.name = 'Arial'
    runFont.size= Pt(8) 
    runFont.underline = WD_UNDERLINE.DOUBLE
    runFont.italic = True
    # set to NR to New Row 
    curRowStatusFlags = 'NR,'      
    #
    # Declare this new element as an endpoint ?
    #
    if diffXpath.line in newEPXpathList :
        curRowControlFlags = 'EP,'
        # take last paragraph with component type 
        wrkLastPar = ps[0]
        # Add the text before to describe as an End Point 
        newPar = wrkLastPar.insert_paragraph_before(text='See MDR for sub elements') 
        runFont = newPar.runs[0].font 
        runFont.color.rgb = greenColor
        runFont.name = 'Arial'
        runFont.size= Pt(8) 
        runFont.underline = WD_UNDERLINE.DOUBLE
     
        
   
    
 
"""
    An implementation of the Myers diff algorithm

    See http://www.xmailserver.org/diff2.pdf
    
    Thanks to Adam New 123456, 
    https://gist.github.com/adamnew123456/37923cf53f51d6b9af32a539cdfa7cc4
    
    This is the algo use by Git Diff to found difference in source code.  We will use it to compare message version's  
    
"""

# These define the structure of the history, and correspond to diff output with
# lines that start with a space, a + and a - respectively.
Keep = namedtuple('Keep', ['line'])
Insert = namedtuple('Insert', ['line'])
Remove = namedtuple('Remove', ['line'])

# See frontier in myers_diff
Frontier = namedtuple('Frontier', ['x', 'history'])

def myers_diff(a_lines, b_lines):
    
    # This marks the farthest-right point along each diagonal in the edit
    # graph, along with the history that got it there
    frontier = {1: Frontier(0, [])}

    def one(idx):
        """
        The algorithm Myers presents is 1-indexed; since Python isn't, we
        need a conversion.
        """
        return idx - 1

    a_max = len(a_lines)
    b_max = len(b_lines)
    for d in range(0, a_max + b_max + 1):
        for k in range(-d, d + 1, 2):
            # This determines whether our next search point will be going down
            # in the edit graph, or to the right.
            #
            # The intuition for this is that we should go down if we're on the
            # left edge (k == -d) to make sure that the left edge is fully
            # explored.
            #
            # If we aren't on the top (k != d), then only go down if going down
            # would take us to territory that hasn't sufficiently been explored
            # yet.
            go_down = (k == -d or 
                    (k != d and frontier[k - 1].x < frontier[k + 1].x))

            # Figure out the starting point of this iteration. The diagonal
            # offsets come from the geometry of the edit grid - if you're going
            # down, your diagonal is lower, and if you're going right, your
            # diagonal is higher.
            if go_down:
                old_x, history = frontier[k + 1]
                x = old_x
            else:
                old_x, history = frontier[k - 1]
                x = old_x + 1

            # We want to avoid modifying the old history, since some other step
            # may decide to use it.
            history = history[:]
            y = x - k

            # We start at the invalid point (0, 0) - we should only start building
            # up history when we move off of it.
            if 1 <= y <= b_max and go_down:
                history.append(Insert(b_lines[one(y)]))
            elif 1 <= x <= a_max:
                history.append(Remove(a_lines[one(x)]))

            # Chew up as many diagonal moves as we can - these correspond to common lines,
            # and they're considered "free" by the algorithm because we want to maximize
            # the number of these in the output.
            while x < a_max and y < b_max and a_lines[one(x + 1)] == b_lines[one(y + 1)]:
                x += 1
                y += 1
                history.append(Keep(a_lines[one(x)]))

            if x >= a_max and y >= b_max:
                # If we're here, then we've traversed through the bottom-left corner,
                # and are done.
                return history
            else:
                frontier[k] = Frontier(x, history)

    assert False, 'Could not find edit script'
    '''
    end of code from  https://gist.github.com/adamnew123456/37923cf53f51d6b9af32a539cdfa7cc4
    '''

def set_col_widths(table):
    widths = (Cm(6), Cm(2), Cm(6))
    for row in table.rows:
        for idx, width in enumerate(widths):
            row.cells[idx].width = width

   
    
inputMessage = ''
msgData  = ''
dataPath = ''
flagOrColumn = False
mugDocument = ''
oldMugDir = ''
newMugDir  = ''
oldCtrlDir = ''
newCtrlDir = ''
pathDictN_to_T = ''
pathDictNexo = ''
diffReportDir = '' 
 
endPointList = []     # used to add new end points 
xmlTagDictN_to_T = {}  # used to get tag from name 
xmlTagDictNexo = {}  # From the Xpath, give info on the message component
oldCtrlDictionary = {}
newCtrlDictionary = {}

msgData = '' 

blueColor     = RGBColor(0, 112, 192)
lightBlueColor= RGBColor(125, 199, 192)
paleGreyColor = RGBColor(199, 199, 199)  
greenColor    = RGBColor(0, 162, 73)
lightGreenColor= RGBColor(75, 255, 156)
blackColor    = RGBColor(0, 0, 0)
redColor      = RGBColor(222, 0, 0)
lightRedColor = RGBColor(255, 139, 139)   

fparam = open('updateMUGTable.ini')

documentName = ''
now = datetime.datetime.now()
        
       
print(now.strftime("%Y-%m-%d %H:%M:%S") + ' - Starting Updating  Message Table ')

for paramLine in fparam:
    if not paramLine[0] == '#':
        parseParam()
    if not pathDictN_to_T == '' :  
        fcode = open(pathDictN_to_T)
        xmlTagDictN_to_T = json.loads(fcode.read() )
        fcode.close()
        pathDictN_to_T = '' 
    
    if not pathDictNexo == '' :  
        fcode = open(pathDictNexo)
        xmlTagDictNexo = json.loads(fcode.read() )
        fcode.close()
        pathDictNexo = '' 
        
    if not newMugDir == '' :
        if not os.path.exists(newMugDir):
            os.makedirs(newMugDir)
    
    if not newCtrlDir == '' :
        if not os.path.exists(newCtrlDir):
            os.makedirs(newCtrlDir)

    if not diffReportDir == '' :
        if not os.path.exists(diffReportDir):
            os.makedirs(diffReportDir)



    if not mugDocument == '' :
        
        mugDocx = Document(oldMugDir+mugDocument)
        mugTables = mugDocx.tables
        mugDocumentSave = mugDocument 
        mugDocument = ''
    
                
        # Set tab in msg table. 
        #
        wrkDocStyles = mugDocx.styles         
              
        wrkPf  = wrkDocStyles['MessageTable'].paragraph_format
        wrkTabs = wrkPf.tab_stops
        wrkTabs.clear_all()
        for n in range(12):
            wrkTabs.add_tab_stop((n+1)*Cm(0.2)) 
            
        # Create the doc for report 
        diffReportDocx = Document()     
        diffRptStyle = diffReportDocx.styles['Normal']
        font = diffRptStyle.font
        font.name = 'Calibri'
        font.size= Pt(11)

       
    if not msgData == '' :
        fcode = open(dataPath+msgData)
        print('Opening '+dataPath+ msgData )    
        
        curLevel = 0 
        prevLevel = 0     
        xmlTagList = [] 
        
             
        
        
        '''
          XMLTagList will provide the list of xml element in the new message.  This is en extract from the repository. 
          For each element, we have a Python Dict with 
              "Xpath": The XPATH of the element, from the root "Document" 
              "Xpathlist" : The XPATH, but in the form of a list
              "Name": The name of the element ( Long name as seen in the MUG) 
              "Level": The level, The Document, root, is 0.  
              "Tag":  The TAG, or short name of the element 
              "Type": The type of component of datatype. 
              "Mult": The multiplicity
        
        
        
        '''
                        
        xmlTagList = json.loads(fcode.read() )
        fcode.close()
       
         
        msgData ='' 
           
    if not inputMessage == '' :    
        
        now = datetime.datetime.now()
        
        print(' Starting at ' + now.strftime("%Y-%m-%d %H:%M:%S"))
        #
        # Search message table in MUG.  We will find it bby testing first row.  The message table will have Lvl in the first column and the message name 
        # in the second one, except for message with a Or column, in this case, this is column 3. 
        #
        lvlIndex = 0   # start with level
        if flagOrColumn:
            msgNameIndex = 2 
        else:
            msgNameIndex = 1      
        
        lenMugTables = len(mugTables)
        mugTableIndex = 0 
        flagFound = False
        
        while (mugTableIndex < lenMugTables  ) and (not flagFound) :
            
            tblCells = mugTables[mugTableIndex].rows[0].cells
            if len(tblCells) > 5: # message tbl are at least 6 column ...
                txtLvl = tblCells[lvlIndex].text.strip()
                txtMsgName = tblCells[msgNameIndex].text.strip() 
                if (txtLvl == 'Lvl') and (txtMsgName == inputMessage ):
                    msgTable = mugTables[mugTableIndex]
                    flagFound = True
            mugTableIndex += 1
    
        if (not flagFound):
            print( ' Table not found for message : ' + inputMessage) 
            exit()
#           
        nbrOfColumn = len(msgTable.rows[0].cells)
              
        
        print(' Nbr of column ' + str(len(msgTable.rows[0].cells)))
        #
        # We need to reload old control dictionary 
        #
  
        fcode = open(oldCtrlDir+inputMessage+'.json')
        oldCtrlDictionary = json.loads(fcode.read() )
        fcode.close()
    
        
  
        #       
        isEndPointActive = False
        endPointXpath = []
                       
        '''
        Set colums index in message table. Some table have a Or in position 2
        Other not
        '''
        
        msgTableLevelColIndex = 0
        if nbrOfColumn == 6:
            
            msgTableNameColIndex = 1 
            msgTableMultColIndex = 2 
            msgTableRuleColIndex = 3 
            msgTableCstrColIndex = 4 
            msgTableDescColIndex =5
           
        elif nbrOfColumn == 7 :
            msgTableNameColIndex = 2 
            msgTableMultColIndex = 3 
            msgTableRuleColIndex = 4 
            msgTableCstrColIndex = 5 
            msgTableDescColIndex=6
           
        else :
            print( ' Unknown number of columns ')
            exit()
        '''
        Point to first entry in message table
        Point to second entry in new message, we start at the Header, Document/MessageName is not in message table
        '''
        #msgTableIndex=1  # start after the header   
        #maxTableEntry = len( msgTable.rows )
        xmlTableIndex = 1   
        xmlItem = xmlTagList[xmlTableIndex]
        maxXmlTableEntry = len(xmlTagList)
        
        #tabsSetting = msgTable.rows[2].cells[msgTableNameColIndex].paragraphs[0].paragraph_format.tab_stops 
        
        '''
        
        
        To find difference, we will build two list of xpath 
        We the new version list is built from repository extract, Active endpoind are not include in the list.  
        We also test to see if new end point need to be added.  In this case, a list of new EP xpath is built to keep the information, so when the new row 
        will be created, we will be able to set them as new end point. 
        
        We will compare the lists
        
        Removed Item 
            if under a new EP
                Remove from table
            else    
                Mask as deleted in the table.  
                
        New row : Add as new row in message table
        
        Row in both, 
            compare Multiplicity and type to see if there is modification, using info from dictionary 
        
        
        '''
        '''
         
         To build the first list, we will recreate xpath of actual component in the MUG Message Table
         While doing this, we need to remove 
         old Deleted ROW if they are still there and reset field still mark as updated 
         from previous update
         
        '''
        msgTableXpath= []
        curLevel= 0
        prevLevel = 0
        msgTableLvl = 0
        msgTableCount = 1

        msgTableXpath.append(xmlTagList[0]["Xpathlist"][0])  # tag Document
        msgTableXpath.append(xmlTagList[0]["Xpathlist"][1])  # tag of message name 
        flagFirstRow = True
        activeEPXpath = []
        curMsgXpathList = []
        now = datetime.datetime.now()    
        print( now.strftime("%Y-%m-%d %H:%M:%S") + '-  Starting Step 1 - MUG analysis  ')

        for curMsgRow in msgTable.rows:
            if not flagFirstRow:
                currentMsgName = curMsgRow.cells[msgTableNameColIndex].text
                prevLevel = curLevel
                curLevel=  int( curMsgRow.cells[msgTableLevelColIndex].text )
                # 
                # In old table, some Tab are not ok.  Maybe remove later, if all tab are ok in all MUG
                #
                nbTab = currentMsgName.count('\t')
                if (curLevel != nbTab +1):
                    currentMsgName = currentMsgName.replace('\t', '')
                    print( ' invalid number of tab in element : ' + currentMsgName + 'at index ' + str(msgTableCount))
                    for n in range(curLevel-1):
                        currentMsgName = '\t' + currentMsgName
                    curMsgRow.cells[msgTableNameColIndex].text = currentMsgName 
                # Remove all useless car from name 
                #
                currentMsgName = currentMsgName.replace('\t', '')
                #currentMsgName = currentMsgName.replace('\xf0','')
                currentMsgName = currentMsgName.replace('-','')
                #currentMsgName = currentMsgName.replace('\xA0',' ')
                #currentMsgName = currentMsgName.replace('\xad','')
                currentMsgName = currentMsgName.strip()
                #print( ' Processing tag name ' + currentMsgName + ' at index ' + str(msgTableIndex))
                now = datetime.datetime.now()      
                if (msgTableCount % 100) == 0:
                    print( now.strftime("%Y-%m-%d %H:%M:%S") + '-  Processing Step 1 - MUG analysis at  ' + currentMsgName + ' at index ' + str(msgTableCount))
                currentMsgTag = xmlTagDictN_to_T[ currentMsgName ]
                
                
                tmpLevel = prevLevel
                while (tmpLevel >= curLevel):
                    msgTableXpath.pop()   
                    tmpLevel -= 1 
                msgTableXpath.append(currentMsgTag)
                #
                leftIndent = 0
                if curLevel >= 2:   
                    leftIndent = (curLevel-1) * Cm(0.2)  
    
                #
                #
                if len(curMsgRow.cells[msgTableNameColIndex].paragraphs) > 1 :
                    print(  '   More than 1 paragraph in element : ' + currentMsgName + ' at index ' + str(msgTableCount))
               
                
                wrkPar = curMsgRow.cells[msgTableNameColIndex].paragraphs[0]
                wrkPf = wrkPar.paragraph_format
                wrkPf.first_line_indent = -leftIndent 
                wrkPf.left_indent = leftIndent
                wrkPar.runs[0].font.bold = True
                
                curMsgRow.cells[msgTableLevelColIndex].paragraphs[0].paragraph_format.alignment = WD_PARAGRAPH_ALIGNMENT.CENTER 
  
                wrkTabs = wrkPf.tab_stops
                wrkTabs.clear_all()
                #for n in range(12):
                #    wrkTabs.add_tab_stop((n+1)*Cm(0.2)) 
                
                
                # rebuilt the xpath  
                #
                msgTableXpathString = ''
                for mTag in msgTableXpath:
                    msgTableXpathString = msgTableXpathString + '/' + mTag
                #
                # remove last '/'
                #    
                msgTableXpathString = msgTableXpathString [1:]
                #print( ' xpath from mug table : ' + msgTableXpathString )    
                if msgTableXpathString not in oldCtrlDictionary:
                    print('= ERROR =========================================')
                    print( ' xpath Not on old control dictionary  : ' + msgTableXpathString )    
                    print('==========================================')
                  
                    #exit()
                curRowStatusFlags = oldCtrlDictionary[msgTableXpathString]['Status']
                curRowControlFlags = oldCtrlDictionary[msgTableXpathString]['Control']
                #
                # do we have a new Not used Row, during MUG edition, the editor may decide to 
                # a row to grey.  Cannot be a New rows, this has to be done in a MUG with final status. 
                # Look with TWG to see how to set to Not used with key word put in description or in Rule or CTRL table. 
                if ( not 'NR' in curRowStatusFlags) and (not 'NU' in curRowControlFlags):
                    currentMsgNamePar = curMsgRow.cells[msgTableNameColIndex].paragraphs
                    curMParRun = currentMsgNamePar[0].runs[0]
                    if curMParRun.font.color.type is not None: # if someone play with color, this is an unused element
                        curRowControlFlags = curRowControlFlags + 'NU,'
                            
                if 'DR' in curRowStatusFlags :
                    remove_row(msgTable,curMsgRow )
                else:            
                    if 'NU' in curRowControlFlags :
                        flagNU = True
                    else:
                        flagNU = False
                    for wrkCell in curMsgRow.cells: # this will reset new rows marks (green) and updated marks (blue)
                        for wrkPar in wrkCell.paragraphs:
                            if  wrkPar.style != 'MessageTable': 
                                wrkPar.style = 'MessageTable'  
                            for wrkRun in wrkPar.runs:
                                runFont = wrkRun.font
                                runFont.underline = None
                                runFont.color.theme_color = None  # reset color to style default
                                if flagNU:
                                    runFont.color.rgb  = paleGreyColor
                    #
                    #
                    #  Now we can create with these entry the list of component to compare 
                    #  with the new version of the message 
                    #
                    # 
                    if 'EP' in curRowControlFlags :
                        activeEPXpath.append(msgTableXpathString)  
                    curMsgXpathList.append(msgTableXpathString)
            else:
                flagFirstRow = False   # I don't want the top of the table
            msgTableCount += 1         # use to send a line every 100 entry, useful in big message to have a feed back
                

        newMsgXpathList = []
        isEndPointActive = False
        endPointXpath = '' 
        newEPXpathList = []
        print ( ' Message table elements list ready ')
        #pprint ( curMsgXpathList  )
        
        print ( 'Active End Points Xpath  : ' + str(activeEPXpath) )
     
        now = datetime.datetime.now()    
        print( now.strftime("%Y-%m-%d %H:%M:%S") + '-  Starting Step 2 - New message extraction  ')

        xmlTableCount = 1
        for xmlItem in xmlTagList:
            xmlXpath = u''
            xmlXpath = unicode(xmlItem["Xpath"]) 
            now = datetime.datetime.now() 
            if (xmlTableCount % 100) == 0:
                    print( now.strftime("%Y-%m-%d %H:%M:%S") + '-  Processing Step 2 - New message extraction  ' + xmlXpath + ' at index ' + str(xmlTableCount))
            if not isEndPointActive:
                if xmlXpath in activeEPXpath:
                    isEndPointActive = True
                    endPointXpath = xmlXpath
                else: 
                    tmpMsgType = xmlItem["Type"]
                    lastc = tmpMsgType [-1]        
                    while lastc.isdigit():
                        tmpMsgType = tmpMsgType[:-1]
                        lastc = tmpMsgType [-1]
                    #
                    # Declare this element as an endpoint ?
                    #
                    if tmpMsgType in endPointList:
                        isEndPointActive = True
                        endPointXpath = xmlXpath 
                        newEPXpathList.append(xmlXpath)
               
                newMsgXpathList.append(xmlXpath)
            else:    # end point is active 
                if not xmlXpath.startswith(endPointXpath): # end of end point 
                    isEndPointActive = False
                    endPointXpath = '' 
                    newMsgXpathList.append(xmlXpath)
                    #
                    # we have to check if a new EP is starting either by the current EP of message table 
                    # or new EP type. 
                    #                    
                    if xmlXpath in activeEPXpath:
                        isEndPointActive = True
                        endPointXpath = xmlXpath 
                    else: 
                        tmpMsgType = xmlItem["Type"]
                        lastc = tmpMsgType [-1]        
                        while lastc.isdigit():
                            tmpMsgType = tmpMsgType[:-1]
                            lastc = tmpMsgType [-1]
                        #
                        # Declare this element as an endpoint ?
                        #
                        if tmpMsgType in endPointList:
                            isEndPointActive = True
                            endPointXpath = xmlXpath 
                            newEPXpathList.append(xmlXpath)
            
            xmlTableCount += 1        
                    
                    
                    
                        
                            
        newMsgXpathList.pop(0) # remove Document/MessageName, we start at the header 
   
          
         
   
        print ( 'New  Msg list  from repository data  ready  '  )
        #pprint (newMsgXpathList )
      
        print ( 'New  EP list   : '  )
        pprint (newEPXpathList )
   
        #
        # and then, a miracle occur... 
        # this will provide the difference between the two list,  but in the best way to show difference, like in a source compare tool. 
        # this is the algo used by GitDiff
        #
        diff = myers_diff(curMsgXpathList,newMsgXpathList)
        #
        # we keep the diff file for debug... 
        #
        if not diffReportDir == '':
            df = open(diffReportDir+inputMessage+'.diff.txt','w')
            for elem in diff:
                if isinstance(elem, Keep):
                    df.write(' ' + elem.line+'\n')
                elif isinstance(elem, Insert):
                    df.write('+' + elem.line+'\n')
                else:
                    df.write('-' + elem.line+'\n')
            df.close()
            
            
        '''
        Processing of diff file 
        set msgTableIndex to 1
        for each line in diff file 
            if elem is Keep  (line is not change)
                
                using definition from dictionary
                Verify if Mult, type are change, if yes do the change and mark revision
                For a change MU = Multiplicity updated, TU = Type Update  
                
            elif elem is remove 
                set current row as removed ( set DR in status for Deleted Row)
                
            else ( new element )
                add new element in table
                      
            add 1 to msgTableIndex
        
        
        ''' 
        now = datetime.datetime.now()    
        print( now.strftime("%Y-%m-%d %H:%M:%S") + '-  Starting Step 3 - MUG update ') 
        '''
        
        Before processing the difference, we will create a new section into difference report 
        
             
        
        '''
        
        diffTitleName = 'Message ' + inputMessage
          

               
        diffReportDocx.add_heading(diffTitleName, 1)
                  
        diffTable = diffReportDocx.add_table(rows=1, cols=3)
        set_col_widths(diffTable) 
        diffTable.style = 'LightList-Accent1'
        hdr_cells = diffTable.rows[0].cells
        hdr_cells[0].text = 'Message component '
        hdr_cells[1].text = ' '
        hdr_cells[2].text = 'Description '
 
 
 
            
        msgTableIndex=1  # we start after the header line of the table     
        for diffXpath in diff:
            now = datetime.datetime.now() 
            if (msgTableIndex % 100) == 0:
                    print( now.strftime("%Y-%m-%d %H:%M:%S") + '-  Processing Step 3 - MUG update at  ' + diffXpath.line + ' at index ' + str(msgTableIndex))
                
            diffRptXpath = diffXpath.line.replace('/', '/\n')
                            
            if isinstance(diffXpath, Keep):
                # we are at the same entry  
                #
                xmlDictItem = xmlTagDictNexo[diffXpath.line]
                #print( ' Keep row from dict :' + str(xmlDictItem ))
                # we are at the same entry  
                wrkCells = msgTable.rows[msgTableIndex].cells
                #
                #
                #
                
                curRowStatusFlags = ''
                curRowControlFlags = oldCtrlDictionary[diffXpath.line]['Control']
                flagNU = False
                if 'NU' in curRowControlFlags:
                    flagNU = True
                #  Compare Multiplicity 
                #  if updated, mark as modified in blue and set status to MU - Multiplicity Updated
                #
                msgTableMult = wrkCells[msgTableMultColIndex].text
                if msgTableMult != xmlDictItem["Mult"]:
                    print(' Multiplicity of ' + xmlDictItem["Name"] + ' to updated from ' + msgTableMult + ' to ' + xmlDictItem["Mult"])
                    wrkCells[msgTableMultColIndex].text = xmlDictItem["Mult"]
                    runFont = wrkCells[msgTableMultColIndex].paragraphs[0].runs[0].font
                    if flagNU:
                        runFont.color.rgb = lightBlueColor
                    else:
                        runFont.color.rgb = blueColor
                    runFont.name = 'Arial'
                    runFont.size= Pt(8) 
                    runFont.underline = True
                    curRowStatusFlags = curRowStatusFlags + 'MU,'
                    wrkCells[msgTableMultColIndex].paragraphs[0].paragraph_format.alignment = WD_PARAGRAPH_ALIGNMENT.CENTER
                    
                    diffRow_cells =diffTable.add_row().cells
                    diffRow_cells[0].text = diffRptXpath
                    diffRow_cells[1].text = ' # '
                    diffRow_cells[2].text = ' Multiplicity changed from ' + msgTableMult + ' to ' + xmlDictItem["Mult"]
                    
                #
                #   point to last paragraph of description 
                #   Get Tag and Datatype
                #
                #  Compare tag and Type in the last line aka run of the usage/description 
                #  if updated, mark as modified in blue and set status to TU - Type Updated
                #
                currentMsgDescLastPar = wrkCells[msgTableDescColIndex].paragraphs[-1]  
                currentMsgTagDataType = ''
                if len(currentMsgDescLastPar.runs)  > 0 :
                    currentMsgTagDataType = currentMsgDescLastPar.runs[-1].text     # -1, we want the last last run !    
                lastc = xmlDictItem["Type"][-1]        
                if lastc.isdigit():
                    typeElem = '>'
                else:
                    typeElem = '>::'+xmlDictItem["Type"]                
                xmlTableTagDataType  = '<'+xmlDictItem["Tag"]+typeElem 
                
                if len(currentMsgTagDataType) > 0:
                    if currentMsgTagDataType[0] == '\n':
                        xmlTableTagDataType  = '\n' + xmlTableTagDataType
                if currentMsgTagDataType != xmlTableTagDataType:
                    # something is changed...      
                    print(' Datatype  of ' + xmlDictItem["Name"] + ' to updated from ' + currentMsgTagDataType + ' to ' + xmlTableTagDataType)   
                                            
                               
                    currentMsgDescLastPar.runs[-1].text = xmlTableTagDataType
                    runFont = currentMsgDescLastPar.runs[-1].font                    
                    runFont.name = 'Arial'
                    runFont.size= Pt(8) 
                    runFont.italic = True
                    if flagNU:
                        runFont.color.rgb = lightBlueColor
                    else:
                        runFont.color.rgb = blueColor
                    runFont.underline = True
                    curRowStatusFlags = curRowStatusFlags +'TU,'    
                                     
                    diffRow_cells =diffTable.add_row().cells
                    diffRow_cells[0].text = diffRptXpath
                    if currentMsgTagDataType.endswith('Code'):
                        diffRow_cells[1].text = ' CS '
                        diffRow_cells[2].text = ' Codeset updated from ' + currentMsgTagDataType + ' to ' + xmlTableTagDataType
                    else:
                        diffRow_cells[1].text = ' DT '
                        diffRow_cells[2].text = ' Datatype updated from ' + currentMsgTagDataType + ' to ' + xmlTableTagDataType             
                #
               
                    
            elif isinstance(diffXpath, Remove):
                wrkCells = msgTable.rows[msgTableIndex].cells
                curRowControlFlags = oldCtrlDictionary[diffXpath.line]['Control']
                flagNU = False
                if 'NU' in curRowControlFlags:
                    flagNU = True
                for tblCell in wrkCells:
                    for tblPar in tblCell.paragraphs:
                        for tblRun in tblPar.runs:
                            if flagNU:
                                tblRun.font.color.rgb = lightRedColor
                            else:
                                tblRun.font.color.rgb = redColor
                            tblRun.font.double_strike = True
                curRowStatusFlags =  'DR,'
                curRowControlFlags = ''    # row will be deleted, no more and endpoint or a Not Used... 
                diffRow_cells =diffTable.add_row().cells
                diffRow_cells[0].text = diffRptXpath
                diffRow_cells[1].text = ' - '
                diffRow_cells[2].text = ' Deleted '    
                
                    
            else: # it's an insert
                curRowStatusFlags  = ''
                curRowControlFlags = ''  
                xmlDictItem = xmlTagDictNexo[diffXpath.line]
                addXMLEntryInMsgTable()  
                diffRow_cells =diffTable.add_row().cells
                diffRow_cells[0].text = diffRptXpath
                diffRow_cells[1].text = ' + '
                diffRow_cells[2].text = ' New component  '     
                
        
            newCtrlDictionary[diffXpath.line] = {'Status':curRowStatusFlags, 'Control' : curRowControlFlags }
            msgTableIndex += 1
            #print( ' next index :'+ str(msgTableIndex))
        
        
        mugDocx.save(newMugDir +mugDocumentSave )  
        diffReportDocx.save(diffReportDir+ 'DiffReport_' +mugDocumentSave)
        
        fcode = open(newCtrlDir+inputMessage+'.json', 'w')
        
        json.dump(newCtrlDictionary, fcode)
        fcode.close()
              
        oldCtrlDictionary = {}
        newCtrlDictionary = {}
        
        now = datetime.datetime.now()
        print(' Message Table ' + inputMessage + ' update finished  at ' + now.strftime("%Y-%m-%d %H:%M:%S"))  
        inputMessage = ''
              
       
#topCommandFile.close()
now = datetime.datetime.now()
        
         
print('<< end of processing >>  - '+ now.strftime("%Y-%m-%d %H:%M:%S"))



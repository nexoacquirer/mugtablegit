#!/usr/local/bin/python2.7
# encoding: utf-8
'''
Created on 21 mai 2018

@author: martinprovost




Be patient, during processing, a line is printed at every 100 rows, and it can take 5 to 10 minutes to process 100 rows. 

'''

from __future__ import print_function # Py2 compat

import re
import os
import json
import subprocess
import time
import datetime

from collections import namedtuple
import sys
from docx import Document
from docx import *
from docx.shared import Pt
#from docx.enum.section import WD_ORIENT
from docx.shared import Cm
from docx.shared import RGBColor
from docx.text.tabstops import TabStop, TabStops
from docx.enum.text import WD_ALIGN_PARAGRAPH

import lxml
from lxml import etree


from pprint import pprint


def parseParam():
    print(' Parsing : '+paramLine )
    paramValue = re.split(' ', paramLine)
    
    print(' Param Value  : '+  str(paramValue) )
  
     
    global inputMessage
    global msgData  
    global dataPath
    global mugDocument
    global oldMugDir
    global newMugDir 
    global oldCtrlDir
    global newCtrlDir 
    global pathDictN_to_T
    global pathDictNexo
    global endPointList
    global flagOrColumn
    global baseDoc
    

     
    for index in range(len(paramValue)):
        if len(paramValue[index]) != 0:
            if paramValue[index][-1] == '\n':
                paramValue[index] = paramValue[index][:-1]
        if len(paramValue[index]) != 0:
            if paramValue[index][-1] == '\r':
                paramValue[index] = paramValue[index][:-1]
        splitParam = re.split('=', paramValue[index])
       
        
        if splitParam[0] == 'MESSAGE' :
            inputMessage = splitParam[1]
            flagOrColumn = False     
        if splitParam[0] == 'MSGDATA':
            msgData = splitParam[1]  
        if splitParam[0] == 'MSGDATADIR':
            dataPath = splitParam[1]  
        if splitParam[0] == 'MUGDOC':
            mugDocument  = splitParam[1]  
        if splitParam[0] == 'OLDMUGDIR':
            oldMugDir = splitParam[1]  
        if splitParam[0] == 'NEWMUGDIR':
            newMugDir = splitParam[1]  
        if splitParam[0] == 'OLDCTRLDIR':
            oldCtrlDir = splitParam[1]  
        if splitParam[0] == 'NEWCTRLDIR':
            newCtrlDir = splitParam[1]     
            
        if splitParam[0] == 'DICT_Name_to_Tag':
            pathDictN_to_T = splitParam[1]    
        if splitParam[0] == 'DICT_Nexo':
            pathDictNexo = splitParam[1]            
        if splitParam[0] == 'ENDPOINT':
            endPointList.append(splitParam[1])
        if splitParam[0] == 'FLAGOR':
            flagOrColumn = True   
        if splitParam[0] == 'BASEDOC':
            baseDoc = splitParam[1]

            

    print(' Param parsed: ' + paramLine )        


def remove_row(table, row):
    tbl = table._tbl
    tr = row._tr
    tbl.remove(tr)            

def  addXMLEntryInMsgTable():   
    global msgTableIndex
    global maxTableEntry
     
    global xmlTableIndex 
    global maxXmlTableEntry
    global tabsSetting
    global endPointList
    global isEndPointActive 
    global endPointXpath 
    global diffXpath
    global curRowStatusFlags  
    global curRowControlFlags
    global msgTable
    global msgTableNameColIndex 
    global msgTableMultColIndex  
    global msgTableRuleColIndex 
    global msgTableCstrColIndex  
    global msgTableDescColIndex  
    global msgTableLevelColIndex
    
    #print( 'Line to add :' + diffXpath.line )
    #print( '   at index :'+ str(msgTableIndex))
    # add an entry a the end with add_row    
    newRow = msgTable.add_row()
    # copy the empty row before common row
    # this is done directly using lxml.xtree function 
    # 
    #  
    #_newRow = newRow._tr
    #_previousRow = msgTable.rows[msgTableIndex-1]._tr
    #_previousRow.addnext(_newRow) # lxml.tree function to add a sibling
    # delete row at end, we don't need it anymore
    #remove_row(msgTable, msgTable.rows[-1])
    
    # Now, since a Row has been added, msgTableIndex now point to new entry 
    # Initialise new row with new entry, all in green undeline to mark new. 
 

    #wrkRow = msgTable.rows[msgTableIndex]
    wrkRow = newRow
    for wrkCell in wrkRow.cells: #  
        for wrkPar in wrkCell.paragraphs:
            if wrkPar.style != 'MessageTable':  
                wrkPar.style = 'MessageTable'  
    
    xmlDictItem = xmlTagDictNexo[xpathItem]
    
    # compute the level
    #elementList = re.split('[/]', xpathItem)
    #print(' Element list :' + elementList )
        #curLevel = len(elementList) - 2
    
    curLevel = xmlDictItem["Level"]        
    ps = wrkRow.cells[msgTableLevelColIndex].paragraphs
    # Level               
    runTmp = ps[0].add_run(str(curLevel))
    runFont = runTmp.font 
    
    runFont.name = 'Arial'
    runFont.size= Pt(8) 
 
    ps_fmt = ps[0].paragraph_format
    ps_fmt.alignment = WD_ALIGN_PARAGRAPH.CENTER 

    
    
    
    # Set Name, with the tab, according to Level
 
    tabStr = ''
    if curLevel >= 1:
        for n in range(curLevel-1):
            tabStr = tabStr + '\t'
    #row_cells[1].text = tabStr + xmlDictItem[2]
    ps = wrkRow.cells[msgTableNameColIndex].paragraphs
    #font.color = blackColor
    
    
    #
    #  And soft Hyphen before capital letter in message name
    #
    #msgNewName = xmlDictItem[3].encode('utf-8')
    '''
    Doesn't work, the unicode soft hyphen is not converted to offices hypen ! 
    msgNewName = xmlDictItem[3]
    n = 0
    maxLenName =  len(msgNewName)
    while (n < maxLenName):
        c = msgNewName[n]
        if c.isupper() and n > 4:
            msgNewName = msgNewName[:n] + u'\xAD'  +msgNewName[n:] 
            n += 2
        n += 1  
    '''
    
    
    runTmp = ps[0].add_run(tabStr + xmlDictItem["Name"]) 
    runFont = runTmp.font 
    runFont.bold = True 
   
    runFont.name = 'Arial'
    runFont.size= Pt(8) 
    
    
 
                        
   
    
    leftIndent = 0
    if curLevel >= 2:   
        leftIndent = (curLevel-1) * Cm(0.2)  
    
    #msgTable.rows[msgTableIndex].cells[msgTableNameColIndex].paragraphs[0].paragraph_format.tab_stops = tabsSetting
    wrkPf = wrkRow.cells[msgTableNameColIndex].paragraphs[0].paragraph_format
    wrkPf.first_line_indent = -leftIndent 
    wrkPf.left_indent = leftIndent
 
      
    # Set Mult
    ps = wrkRow.cells[msgTableMultColIndex].paragraphs
    runTmp = ps[0].add_run(xmlDictItem["Mult"]) 
    runFont = runTmp.font 
   
    runFont.name = 'Arial'
    runFont.size= Pt(8) 
    
    ps_fmt = ps[0].paragraph_format
    ps_fmt.alignment = WD_ALIGN_PARAGRAPH.CENTER 
    
    # Empty rule set to blank, but formatted in case of someone add a rule
    ps = wrkRow.cells[msgTableRuleColIndex].paragraphs
    runTmp = ps[0].add_run('') 
    runFont = runTmp.font 
    
    runFont.name = 'Arial'
    runFont.size= Pt(8) 
    
    # Empty constraint set to blank, but formatted in case of someone add a contraint
    ps =wrkRow.cells[msgTableCstrColIndex].paragraphs
    runTmp = ps[0].add_run('') 
    runFont = runTmp.font 
    
    runFont.name = 'Arial'
    runFont.size= Pt(8) 
   
    ps_fmt = ps[0].paragraph_format
    ps_fmt.alignment = WD_ALIGN_PARAGRAPH.CENTER 
    
    #
    # 
    #
    
    
    # Tag and type in bottom of usage
    lastc = xmlDictItem["Type"][-1]        
    if lastc.isdigit():
        typeElem = '>'
    else:
        typeElem = '>::'+xmlDictItem["Type"]      
    ps = wrkRow.cells[msgTableDescColIndex].paragraphs
    runTmp = ps[0].add_run('<'+xmlDictItem["Tag"]+typeElem) 
    runFont = runTmp.font 
    
    runFont.name = 'Arial'
    runFont.size= Pt(8) 
    
    runFont.italic = True
    #
    # Declare this  element as an endpoint ?
    #
    if xpathItem in newEPXpathList :
        curRowControlFlags = 'EP,'
        wrkLastPar = ps[0]
        # Add the text before to describe as an End Point 
        newPar = wrkLastPar.insert_paragraph_before(text='See MDR for sub elements') 
        runFont = newPar.runs[0].font 
        runFont.name = 'Arial'
        runFont.size= Pt(8) 
    
        
    """
    Done for addXMLEntryInMsgTable
    """ 
        
   
    
 

   
    
inputMessage = ''
msgData  = ''
dataPath = ''
flagOrColumn = False
mugDocument = ''
oldMugDir = ''
newMugDir  = ''
oldCtrlDir = ''
newCtrlDir = ''
pathDictN_to_T = ''
pathDictNexo = ''
baseDoc = '' 
 
endPointList = []     # used to add new end points 
xmlTagDictN_to_T = {}  # used to get tag from name 
xmlTagDictNexo = {}  # From the Xpath, give info on the message component
oldCtrlDictionary = {}
newCtrlDictionary = {}

msgData = '' 

blueColor     = RGBColor(0, 112, 192)
lightBlueColor= RGBColor(125, 199, 192)
paleGreyColor = RGBColor(199, 199, 199)  
lightGreyColor = RGBColor(191, 191, 191)  

greenColor    = RGBColor(0, 162, 73)
lightGreenColor= RGBColor(75, 255, 156)
blackColor    = RGBColor(0, 0, 0)
redColor      = RGBColor(222, 0, 0)
lightRedColor = RGBColor(255, 139, 139)   

fparam = open('CreateNewMUGTable.ini')

documentName = ''
now = datetime.datetime.now()
        
       
print(now.strftime("%Y-%m-%d %H:%M:%S") + ' - Starting Updating  Message Table ')

for paramLine in fparam:
    if not paramLine[0] == '#':
        parseParam()
    if not pathDictN_to_T == '' :  
        fcode = open(pathDictN_to_T)
        xmlTagDictN_to_T = json.loads(fcode.read() )
        fcode.close()
        pathDictN_to_T = '' 
    
    if not pathDictNexo == '' :  
        fcode = open(pathDictNexo)
        xmlTagDictNexo = json.loads(fcode.read() )
        fcode.close()
        pathDictNexo = '' 
        
    if not newMugDir == '' :
        if not os.path.exists(newMugDir):
            os.makedirs(newMugDir)
    
    if not newCtrlDir == '' :
        if not os.path.exists(newCtrlDir):
            os.makedirs(newCtrlDir)

    
 

       
    if not msgData == '' :
        fcode = open(dataPath+msgData)
        print('Opening '+dataPath+ msgData )    
        
        curLevel = 0 
        prevLevel = 0     
        xmlTagList = [] 
        
        print( now.strftime("%Y-%m-%d %H:%M:%S") + '-  Starting Step 1 - Message Data parsing  ')     
        
        
        #xmlMsgList.append({"xpath" : xpath, "Level" : curLevel,"Tag" :xpathList[curLevel],"Name" :xmlName, "Mult" :multiplicity,"type" :xmltype,"xpathlist" :xpathList})     
        xmlTagList = json.loads(fcode.read() )
        fcode.close()
       
        
        
 
         
        msgData ='' 
           
    

        newMsgXpathList = []
        isEndPointActive = False
        endPointXpath = '' 
        newEPXpathList = []
        print ( ' Message table elements list ready ')
        #pprint ( curMsgXpathList  )
        
        
     
        now = datetime.datetime.now()    
        print( now.strftime("%Y-%m-%d %H:%M:%S") + '-  Starting Step 2 - New message extraction  ')

        xmlTableCount = 1
        for xmlItem in xmlTagList:
            xmlXpath = u''
            #xmlXpath = unicode(xmlItem['Xpath']) 
            xmlXpath = xmlItem['Xpath']
            xmlTagDictNexo[xmlItem['Xpath']] = xmlItem
            if (xmlTableCount % 100) == 0:
                    now = datetime.datetime.now()
                    print( now.strftime("%Y-%m-%d %H:%M:%S") + '-  Processing Step 2 - New message extraction  ' + xmlXpath + ' at index ' + str(xmlTableCount))
            if not isEndPointActive:
                
                tmpMsgType = xmlItem['Type']
                lastc = tmpMsgType [-1]        
                while lastc.isdigit():
                    tmpMsgType = tmpMsgType[:-1]
                    lastc = tmpMsgType [-1]
                #
                # Declare this element as an endpoint ?
                #
                if tmpMsgType in endPointList:
                    isEndPointActive = True
                    endPointXpath = xmlXpath 
                    newEPXpathList.append(xmlXpath)
               
                newMsgXpathList.append(xmlXpath)
            else:    # end point is active 
                if not xmlXpath.startswith(endPointXpath): # end of end point 
                    isEndPointActive = False
                    endPointXpath = '' 
                    newMsgXpathList.append(xmlXpath)
                    #
                    # we have to check if a new EP is starting either by the current EP of message table 
                    # or new EP type. 
                    #                    
                    
                    tmpMsgType = xmlItem['Type']
                    lastc = tmpMsgType [-1]        
                    while lastc.isdigit():
                        tmpMsgType = tmpMsgType[:-1]
                        lastc = tmpMsgType [-1]
                    #
                    # Declare this element as an endpoint ?
                    #
                    if tmpMsgType in endPointList:
                        isEndPointActive = True
                        endPointXpath = xmlXpath 
                        newEPXpathList.append(xmlXpath)
            
            xmlTableCount += 1        
                    
                    
                    
                        
                            
        newMsgXpathList.pop(0) # remove Document/MessageName, we start at the header 
   
          
         
   
        print ( 'New  Msg list  from repository data  ready  ' + ' at index ' + str(xmlTableCount)  )
        #pprint (newMsgXpathList )
      
        print ( 'New  EP list   : '  )
        pprint (newEPXpathList )
   
        #
        #  Create new doc from Basedoc
        #
        mugDocx = Document(baseDoc)
        mugTables = mugDocx.tables
        msgTable =  mugTables[0]   # First table of baseDoc
    
                
        # Set tab in msg table. 
        #
        wrkDocStyles = mugDocx.styles         
              
        wrkPf  = wrkDocStyles['MessageTable'].paragraph_format
        wrkTabs = wrkPf.tab_stops
        wrkTabs.clear_all()
        for n in range(12):
            wrkTabs.add_tab_stop((n+1)*Cm(0.2)) 
        
        #
        msgTableLevelColIndex = 0
        msgTableNameColIndex = 1 
        msgTableMultColIndex = 2 
        msgTableRuleColIndex = 3 
        msgTableCstrColIndex = 4 
        msgTableDescColIndex = 5
        #  set message name in the header of the table 
        ## 
        MessageName = xmlTagList[0]['Name'][:-3]   # [:-3] because we remove Vnn from the name, so it's the same between version
        
        wrkRow = msgTable.rows[0]
        ps = wrkRow.cells[msgTableNameColIndex].paragraphs
    
    
    
        runTmp = ps[0].add_run(MessageName) 
        runFont = runTmp.font 
        runFont.bold = True 
        runFont.color.rgb = lightGreyColor
        runFont.name = 'Arial'
        runFont.size= Pt(8) 
        runFont.italic = True 
        
        #
        #  Create table, all line are new entry 
        # 
        now = datetime.datetime.now()    
        print( now.strftime("%Y-%m-%d %H:%M:%S") + '-  Starting Step 3 - MUG table creation ') 
 
            
        msgTableIndex=1  # we start after the header line of the table     
        for xpathItem in newMsgXpathList:
            now = datetime.datetime.now() 
            if (msgTableIndex % 100) == 0:
                print( now.strftime("%Y-%m-%d %H:%M:%S") + '-  Processing Step 3 - MUG creation  at  ' + xpathItem + ' at index ' + str(msgTableIndex))
        
            curRowStatusFlags  = ''
            curRowControlFlags = ''  
            addXMLEntryInMsgTable()     
        
            newCtrlDictionary[xpathItem] = {'Status':curRowStatusFlags, 'Control' : curRowControlFlags }
            msgTableIndex += 1
            #print( ' next index :'+ str(msgTableIndex))
        
        
        mugDocx.save(newMugDir + MessageName  +'.docx' )  
        fcode = open(newCtrlDir+ MessageName +'.json', 'w')
        
        json.dump(newCtrlDictionary, fcode)
        fcode.close()
              
        newCtrlDictionary = {}
        
        now = datetime.datetime.now()
        print(' Message Table ' + MessageName + ' creation finished  at ' + now.strftime("%Y-%m-%d %H:%M:%S")+ ' at index ' + str(msgTableIndex))  
        MessageName = ''
              
       
#topCommandFile.close()
now = datetime.datetime.now()
        
         
print('<< end of processing >>  - '+ now.strftime("%Y-%m-%d %H:%M:%S"))




#!/usr/local/bin/python2.7
# encoding: utf-8'''
'''
Created on Jul 8, 2018

@author: DHC3606

Used to EDIT MugTable during MUG Maintenance 

Edit Commands are in the last line of the Rules Column :


NU - Declare this line as Not Used  
USE - Remove Not Used Mark
RM - Remove the line from the table
RS - remove Green and blue mark for new and updated marks in the row
IN - Remove End point Mark in the control file and remove the comment if present. 
     and try to remove "see MDR..." comment  
     Sub element can be reinserted by rerunning the UpdateTable script

For V7, a comment for endpoint will be added, if not already in the cell.  
May be remove later because this comment is added when EP is created by update ou create MUG table script . 




A SETFINAL in the ini file will remove deleted row set in red and remove Green and blue mark for new and updated rows. 

And be patient, during processing, a line is printed at every 100 rows, and it can take 5 to 10 minutes to process 100 rows. 
Processing of the acquirer MUG may take more than 1h30, the acceptor batch transfert is huge and take more than 1 hour. 
'''
from __future__ import print_function # Py2 compat

import re
import os
import json
import subprocess
import time
import datetime


import sys
from docx import Document
from docx import *
from docx.shared import Pt
#from docx.enum.section import WD_ORIENT
from docx.shared import Cm
from docx.shared import RGBColor

from docx.enum.text import WD_PARAGRAPH_ALIGNMENT, WD_UNDERLINE


def remove_row(table, row):
    tbl = table._tbl
    tr = row._tr
    tbl.remove(tr)       


def parseParam():
    print(' Parsing : '+paramLine )
    paramValue = re.split(' ', paramLine)
    
    print(' Param Value  : '+  str(paramValue) )
  
     
    global inputMessage
    global msgData  
    global dataPath
    global mugDocument
    global oldMugDir
    global newMugDir 
    global oldCtrlDir
    global newCtrlDir 
    global pathDictN_to_T
    global pathDictNexo
    global endPointList
    global flagOrColumn
    global flagSetFinal
 
    global diffReportDir
     

     
    for index in range(len(paramValue)):
        if len(paramValue[index]) != 0:
            if paramValue[index][-1] == '\n':
                paramValue[index] = paramValue[index][:-1]
        if len(paramValue[index]) != 0:
            if paramValue[index][-1] == '\r':
                paramValue[index] = paramValue[index][:-1]
        splitParam = re.split('=', paramValue[index])
       
        
        if splitParam[0] == 'MESSAGE' :
            inputMessage = splitParam[1]
            flagOrColumn = False     
        if splitParam[0] == 'MUGDOC':
            mugDocument  = splitParam[1]  
        if splitParam[0] == 'MSGDATA':
            msgData = splitParam[1]  
        if splitParam[0] == 'MSGDATADIR':
            dataPath = splitParam[1]  
        if splitParam[0] == 'OLDMUGDIR':
            oldMugDir = splitParam[1]  
        if splitParam[0] == 'NEWMUGDIR':
            newMugDir = splitParam[1] 
        if splitParam[0] == 'OLDCTRLDIR':
            oldCtrlDir = splitParam[1]  
        if splitParam[0] == 'NEWCTRLDIR':
            newCtrlDir = splitParam[1]     
        if splitParam[0] == 'DICT_Name_to_Tag':
            pathDictN_to_T = splitParam[1]   
        if splitParam[0] == 'FLAGOR':
            flagOrColumn = True   
        if splitParam[0] == 'SETFINAL':
            flagSetFinal = True  
            

    print(' Param parsed: ' + paramLine )        


  


     
        

   
    
inputMessage = ''
msgData  = ''
dataPath = ''
flagOrColumn = False
flagSetFinal = False
mugDocument = ''
oldMugDir = ''
newMugDir  = ''
oldCtrlDir = ''
newCtrlDir = ''
pathDictN_to_T = ''
pathDictNexo = ''
diffReportDir = '' 
 
endPointList = []     # used to add new end points 
xmlTagDictN_to_T = {}  # used to get tag from name 
xmlTagDictNexo = {}  # From the Xpath, give info on the message component
ctrlDictionary = {}
newCtrlDictionary = {}

msgData = '' 

blueColor     = RGBColor(0, 112, 192)
lightBlueColor= RGBColor(125, 199, 192)
paleGreyColor = RGBColor(199, 199, 199)  
greenColor    = RGBColor(0, 162, 73)
lightGreenColor= RGBColor(75, 255, 156)
blackColor    = RGBColor(0, 0, 0)
redColor      = RGBColor(222, 0, 0)
lightRedColor = RGBColor(255, 139, 139)   

fparam = open('EditMUGTable.ini')

documentName = ''
now = datetime.datetime.now()
        
       
print(now.strftime("%Y-%m-%d %H:%M:%S") + ' - Starting Updating  Message Table ')

for paramLine in fparam:
    if not paramLine[0] == '#':
        parseParam()
    
    if not pathDictN_to_T == '' :  
        fcode = open(pathDictN_to_T)
        xmlTagDictN_to_T = json.loads(fcode.read() )
        fcode.close()
        pathDictN_to_T = '' 

        
    if not newMugDir == '' :
        if not os.path.exists(newMugDir):
            os.makedirs(newMugDir)
    
    if not newCtrlDir == '' :
        if not os.path.exists(newCtrlDir):
            os.makedirs(newCtrlDir)

    if not mugDocument == '' :
        
        mugDocx = Document(oldMugDir+mugDocument)
        mugTables = mugDocx.tables
        mugDocumentSave = mugDocument 
        mugDocument = ''
    
    if not msgData == '' :
        fcode = open(dataPath+msgData)
        print('Opening '+dataPath+ msgData )    
        
        curLevel = 0 
        prevLevel = 0     
        xmlTagList = [] 
                 
  
       
        '''
          XMLTagList will provide the list of xml element in the new message.  This is an extract from the repository. 
          For each element, we have a Python Dict with 
              "Xpath": The XPATH of the element, from the root "Document" 
              "Xpathlist" : The XPATH, but in the form of a list
              "Name": The name of the element ( Long name as seen in the MUG) 
              "Level": The level, The Document, root, is 0.  
              "Tag":  The TAG, or short name of the element 
              "Type": The type of component of datatype. 
              "Mult": The multiplicity
        
          Loaded in EditMUGTable to have top level of xpath,  
        
        '''
                        
        xmlTagList = json.loads(fcode.read() )
        fcode.close()
       
         
        msgData ='' 
           
    if not inputMessage == '' :    
        
        now = datetime.datetime.now()
        
        print(' Starting at ' + now.strftime("%Y-%m-%d %H:%M:%S"))
        #
        # Search message table in MUG.  We will find it bby testing first row.  The message table will have Lvl in the first column and the message name 
        # in the second one, except for message with a Or column, in this case, this is column 3. 
        #
        lvlIndex = 0   # start with level
        if flagOrColumn:
            msgNameIndex = 2 
        else:
            msgNameIndex = 1      
        
        lenMugTables = len(mugTables)
        mugTableIndex = 0 
        flagFound = False
        
        while (mugTableIndex < lenMugTables  ) and (not flagFound) :
            
            tblCells = mugTables[mugTableIndex].rows[0].cells
            if len(tblCells) > 5: # message tbl are at least 6 column ...
                txtLvl = tblCells[lvlIndex].text.strip()
                txtMsgName = tblCells[msgNameIndex].text.strip() 
                if (txtLvl == 'Lvl') and (txtMsgName == inputMessage ):
                    msgTable = mugTables[mugTableIndex]
                    flagFound = True
            mugTableIndex += 1
    
        if (not flagFound):
            print( ' Table not found for message : ' + inputMessage) 
            exit()
#           
        nbrOfColumn = len(msgTable.rows[0].cells)
              
        
        print(' Nbr of column ' + str(len(msgTable.rows[0].cells)))
        #
        # We need to reload old control dictionary 
        #
  
        fcode = open(oldCtrlDir+inputMessage+'.json')
        ctrlDictionary = json.loads(fcode.read() )
        fcode.close()
    
        
       
        #       
       
                       
        '''
        Set colums index in message table. Some table have a Or in position 2
        Other not
        '''
        
        msgTableLevelColIndex = 0
        if nbrOfColumn == 6:
            
            msgTableNameColIndex = 1 
            msgTableMultColIndex = 2 
            msgTableRuleColIndex = 3 
            msgTableCstrColIndex = 4 
            msgTableDescColIndex =5
           
        elif nbrOfColumn == 7 :
            msgTableNameColIndex = 2 
            msgTableMultColIndex = 3 
            msgTableRuleColIndex = 4 
            msgTableCstrColIndex = 5 
            msgTableDescColIndex=6
           
        else :
            print( ' Unknown number of columns ')
            exit()
       


        '''
         
         To build the first list, we will recreate xpath of actual component in the MUG Message Table
         While doing this, we need to remove 
         old Deleted ROW if they are still there and reset field still mark as updated 
         from previous update
         
        '''
        msgTableXpath= []
        curLevel= 0
        prevLevel = 0
        msgTableLvl = 0
        msgTableCount = 1

        msgTableXpath.append(xmlTagList[0]["Xpathlist"][0])  # tag Document
        msgTableXpath.append(xmlTagList[0]["Xpathlist"][1])  # tag of message name 
        flagFirstRow = True
        activeEPXpath = []
        curMsgXpathList = []
        now = datetime.datetime.now()    
        print( now.strftime("%Y-%m-%d %H:%M:%S") + '-  Starting Step 1 - MUG analysis  ')
        flagRowDeleted = False ; 

        for curMsgRow in msgTable.rows:
            if not flagFirstRow:
                currentMsgName = curMsgRow.cells[msgTableNameColIndex].text
                prevLevel = curLevel
                curLevel=  int( curMsgRow.cells[msgTableLevelColIndex].text )
                # 
                # In old table, some Tab are not ok.  Maybe remove later, if all tab are ok in all MUG
                #
                nbTab = currentMsgName.count('\t')
                if (curLevel != nbTab +1):
                    currentMsgName = currentMsgName.replace('\t', '')
                    print( ' invalid number of tab in element : ' + currentMsgName)
                    for n in range(curLevel-1):
                        currentMsgName = '\t' + currentMsgName
                    curMsgRow.cells[msgTableNameColIndex].text = currentMsgName 
                # Remove all useless car from name 
                #
                currentMsgName = currentMsgName.replace('\t', '')
                #currentMsgName = currentMsgName.replace('\xf0','')
                currentMsgName = currentMsgName.replace('-','')
                #currentMsgName = currentMsgName.replace('\xA0',' ')
                #currentMsgName = currentMsgName.replace('\xad','')
                currentMsgName = currentMsgName.strip()
                #print( ' Processing tag name ' + currentMsgName + ' at index ' + str(msgTableIndex))
                now = datetime.datetime.now()      
                if (msgTableCount % 100) == 0:
                    print( now.strftime("%Y-%m-%d %H:%M:%S") + '-  Processing  - MUG analysis at  ' + currentMsgName + ' at index ' + str(msgTableCount))
                currentMsgTag = xmlTagDictN_to_T[ currentMsgName ]
                
                wrkRunFont = curMsgRow.cells[msgTableNameColIndex].paragraphs[0].runs[0].font
                if (wrkRunFont.double_strike == True) and ((wrkRunFont.color.rgb == lightRedColor)  or (wrkRunFont.color.rgb == redColor)):
                    flagRowDeleted = True ;     
                
                #if not  flagRowDeleted:
                tmpLevel = prevLevel
                while (tmpLevel >= curLevel):
                    msgTableXpath.pop()   
                    tmpLevel -= 1 
                msgTableXpath.append(currentMsgTag)
                #
                if not  flagRowDeleted:
                #
                    msgTableXpathString = ''
                    for mTag in msgTableXpath:
                        msgTableXpathString = msgTableXpathString + '/' + mTag
                    #
                    # remove last '/'
                    #    
                    msgTableXpathString = msgTableXpathString [1:]
                    #print( ' xpath from mug table : ' + msgTableXpathString )    
                    if msgTableXpathString not in ctrlDictionary:
                        print('= ERROR =========================================')
                        print( ' xpath Not on old control dictionary  : ' + msgTableXpathString )    
                        print('==========================================')
                    else:  
                    #exit()
                        curRowStatusFlags = ctrlDictionary[msgTableXpathString]['Status']
                        curRowControlFlags = ctrlDictionary[msgTableXpathString]['Control']
                    
                #
                flagRemoved = False 
                wrkPar = curMsgRow.cells[msgTableRuleColIndex].paragraphs[-1]
                curRowRuleText = ''
                curRowRuleTextSave = ''
                if len(wrkPar.runs) > 0:
                    curRowRuleText = wrkPar.runs[-1].text 
                    curRowRuleTextSave = wrkPar.runs[-1].text  
         
                if 'NU' in curRowRuleText and not flagRemoved :
                    for wrkCell in curMsgRow.cells: 
                        for wrkPar in wrkCell.paragraphs:
                            for wrkRun in wrkPar.runs:
                                runFont = wrkRun.font
                                if runFont.color.rgb == blueColor:
                                    runFont.color.rgb  = lightBlueColor
                                elif runFont.color.rgb == greenColor :
                                    runFont.color.rgb  = lightGreenColor   
                                else:                               
                                    runFont.color.rgb  = paleGreyColor
                    curRowControlFlags = curRowControlFlags + 'NU,'
                    curRowRuleText = curRowRuleText.replace('NU','')
                if 'RM' in curRowRuleText:
                    remove_row(msgTable,curMsgRow )
                    flagRemoved = True 
                # the USE tell to remove the unused mark on the row    
                if 'USE' in curRowRuleText and not flagRemoved:
                    for wrkCell in curMsgRow.cells: # this will reset new rows marks (green) and updated marks (blue)
                        for wrkPar in wrkCell.paragraphs:
                            for wrkRun in wrkPar.runs:
                                runFont = wrkRun.font
                                if runFont.color.rgb == lightBlueColor :
                                    runFont.color.rgb  = blueColor
                                elif runFont.color.rgb == lightGreenColor  :
                                    runFont.color.rgb  = greenColor
                                else:                               
                                    runFont.color.theme_color = None
                    curRowControlFlags = curRowControlFlags.replace( 'NU,', '' )
                    curRowRuleText = curRowRuleText.replace('USE','')           
                # reset new and updated mark 
                if 'RS' in curRowRuleText and not flagRemoved:
                    for wrkCell in curMsgRow.cells: # this will reset new rows marks (green) and updated marks (blue)
                        for wrkPar in wrkCell.paragraphs:
                            for wrkRun in wrkPar.runs:
                                runFont = wrkRun.font
                                runFont.underline = None
                                runFont.color.theme_color = None  # reset color to style default
                                if 'NU' in curRowControlFlags: 
                                    runFont.color.rgb  = paleGreyColor
                    ctrlDictionary[msgTableXpathString]['Status'] = ''
                    curRowRuleText = curRowRuleText.replace('RS','')
                
                # include an end point 
                if 'IN' in curRowRuleText and not flagRemoved:
                    curRowRuleText = curRowRuleText.replace('IN','')
                    curRowControlFlags = curRowControlFlags.replace( 'EP,' , '') 
                    cellText = curMsgRow.cells[msgTableDescColIndex].text
                    if 'See MDR for sub elements' in cellText:
                        wrkPars = curMsgRow.cells[msgTableDescColIndex].paragraphs
                        if len(wrkPars) >= 2:
                            wrkPar = wrkPars[-2]
                            for run in wrkPar.runs:
                                if 'See MDR for sub elements' in run.text:
                                    run.text = run.text.replace( 'See MDR for sub elements' , '')
                                              
                #  remove flag form Rule, if the row is still there
                
                 
                if not flagRemoved: 
                    if (curRowRuleText != curRowRuleTextSave):
                        wrkPar = curMsgRow.cells[msgTableRuleColIndex].paragraphs[-1]
                        wrkPar.runs[-1].text = curRowRuleText
                    ctrlDictionary[msgTableXpathString]['Control'] = curRowControlFlags  
                    if 'EP' in curRowControlFlags:   # this is an end point 
                        cellText = curMsgRow.cells[msgTableDescColIndex].text
                        if 'See MDR for sub elements' not in cellText:
                            # take last paragraph with component type 
                            wrkLastPar = curMsgRow.cells[msgTableDescColIndex].paragraphs[-1]
                            # Add the text before 
                            newPar = wrkLastPar.insert_paragraph_before(text='See MDR for sub elements') 
                            runFont = newPar.runs[0].font 
                            runFont.name = 'Arial'
                            runFont.size= Pt(8) 
                            # New rows have a NR (new row) in the status flag 
                            if 'NR' in curRowStatusFlags : 
                                runFont.color.rgb  = greenColor
                            if 'NU' in curRowControlFlags: 
                                runFont.color.rgb  = paleGreyColor
                                if 'NR' in curRowStatusFlags : 
                                    runFont.color.rgb  = lightGreenColor
                    
                #
                #  The set final  flag ask to publish a version without revision mark. 
                #  So we delete row with a Deleted  flag if they are still there 
                #  and will reset new rows or uppdated rows.  
                #
                if flagSetFinal and not flagRemoved:          
                    if flagRowDeleted :
                        remove_row(msgTable,curMsgRow )
                         
                    else :            
                        if 'NU' in curRowControlFlags :
                            flagNU = True
                        else:
                            flagNU = False
                        for wrkCell in curMsgRow.cells: # this will reset new rows marks (green) and updated marks (blue)
                            for wrkPar in wrkCell.paragraphs:
                                if  wrkPar.style != 'MessageTable': 
                                    wrkPar.style = 'MessageTable'  
                                for wrkRun in wrkPar.runs:
                                    runFont = wrkRun.font
                                    runFont.underline = None
                                    runFont.color.theme_color = None  # reset color to style default
                                    if flagNU:
                                        runFont.color.rgb  = paleGreyColor
                        # 
                        # reset status 
                        #
                        ctrlDictionary[msgTableXpathString]['Status'] = ''                   
                flagRemoved = False
                flagRowDeleted = False  
            else:
                flagFirstRow = False   # I don't want the top of the table
            msgTableCount += 1         # use to send a line every 100 entry, useful in big message to have a feed back
                

         
       
        
        # save after each message 
        mugDocx.save(newMugDir +mugDocumentSave )  
        fcode = open(newCtrlDir+inputMessage+'.json', 'w')
        
        json.dump(ctrlDictionary, fcode)
        fcode.close()
              
        ctrlDictionary = {}
     
              
     
        
        now = datetime.datetime.now()
        print(' Message Table ' + inputMessage + ' update finished  at ' + now.strftime("%Y-%m-%d %H:%M:%S"))  
        inputMessage = ''
              
       
#topCommandFile.close()
now = datetime.datetime.now()
        
         
print('<< end of processing >>  - '+ now.strftime("%Y-%m-%d %H:%M:%S"))

